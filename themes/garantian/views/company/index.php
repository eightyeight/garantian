
<ul class="nav nav-justified tab" style="margin-bottom: 1em">
    <?php foreach($data as $section):?>
        <li <?php echo $section->id == 1 ? 'class="active"' : '';?>><a href="#<?= $section->id?>" data-toggle="tab"><?php echo $section->title;?></a></li>
    <?php endforeach;?>
</ul>

<!-- Tab panes -->


<div class="tab-content">
    <?php foreach($data as $section):?>
        <div class="tab-pane fade <?php echo $section->id == 1 ? 'active in' : '';?>" id="<?= $section->id;?>" style="font-weight: normal !important; min-height: 500px;
height: 400px;">
            <?php echo $section->content;?>
        </div>
    <?php endforeach;?>
</div>

