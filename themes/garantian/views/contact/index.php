<div class="row">
    <div class="col-lg-4">
        <strong><h3 style="color: green">КОНТАКТЫ</h3></strong>
        <div style="color: gray; font-weigt: normal">
            <p>Телефоны:</p>
            <p>+7 (499) 707-75-85</p>
            <p>+7 (926) 187-42-00</p>
            <p>+7 (903) 005-93-23</p>
            <p>+7 (916) 270-78-23</p>
            <p>+7 (903) 216-89-98</p>
        </div>
        <div>
            <p>E-mail</p>
            <p><a href="mailto:622293@mail.ru">622293@mail.ru</a></p>
        </div>
        <div style="color: gray; font-size: 1.22em;">Написать Письмо:</div>
        <div class="callback">
            <form id="contact-form" class="modal-body" role="form">
                <div class="form-group">
                    <input class="form-control input-lg" type="text" placeholder="Ваше имя"  name="name">
                </div>
                <div class="form-group">
                    <input class="form-control input-lg" type="email" placeholder="Ваш email"  name="mail">
                </div>
                <div class="form-group">
                    <textarea class="form-control input-lg" rows="5" placeholder="Ваше сообщение"  name="message"></textarea>
                </div>
            </form>
            <div class="callbtn">
                <button class="btn btn-default btn-md" type="submit" title="Отправить Email" form="contact-form">Отправить</button>
            </div>
        </div>





    </div>
    <div class="col-lg-8">
        <strong><h3 style="color: green">СХЕМА ПРОЕЗДА</h3></strong>
        <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=JZ_Vu3RHh7IaDAd4ew2rJmpo_jvP1iQ2&width=750&height=450">
        </script>
        <p style="font-weight: normal">Адрес: г. Солнечногорск, ул. Баранова, д. 12, Агентство недвижимости "ГАРАНТИЯ"</p>
    </div>

</div>
