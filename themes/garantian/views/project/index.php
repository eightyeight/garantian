
<div class="main">
    <img src="/uploads/project/Nikol.jpg" style="margin-top: 10px; margin-left: 0px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); width: 100%;">
    <div>

        <div class="ribbon-wrapper">
<!--            <div class="ribbon-front">-->
<!--                АРЕНДА НЕЖИЛОЙ НЕДВИЖИМОСТИ В НИКОЛЬСКОЙ ПАНОРАМЕ<br><span style="font-style: italic; font-size:12px;">от 1500 руб/м2-->
<!--            </div>-->

        </div>





    </div>

    <div class="sidebar_2" style="margin-top: 10px;">
        <img src="/uploads/project/pasport-np.jpg" style="height: 400px;  float: left;">
        <div class="tels"><span class="h3">Новостройка "Никольская панорама", Баранова поз. 8-9.
            <div style="margin-top: 20px">Звоните нам по телефонам:</div>
            <div>8 (903) 005-93-23</div>
            <div>8 (916) 270-78-23</div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="mainbar centre_cont" style="margin-top: 20px;">
        <div class="panoramanik">
            <!--
            <img src="ACCamera_001 (800x533).jpg">
            <img src="ACCamera_1_001 (800x533).jpg">
            <img src="PLAN1170.jpg"><p>
            -->
            <a href="/uploads/project//planb.jpg" class="fbox" rel="chem"><img src="/uploads/project/planm.jpg"></a>



            <h1 style="margin-top: 31px;">Жилой комплекс «Никольская Панорама»</h1>
            <div class="homenik" style="text-align: justify;">
                <a href="/uploads/project/ACCamera_1_001(800x533).jpg" class="fbox"><img src="/uploads/project/ACCamera_1_001(800x533).jpg" style="float:left; height: 300px;" alt="Никольская панорама" title="Никольская панорама"></a>
                <div style="padding: 0px 10px; margin-left: 460px;">
                    Агентство Гарантия предлагает <a href="/" title="Новые квартиры в Солнечногорске">новые квартиры в Солнечногорске</a>. В центре г. Солнечногорск на ул. Баранова началось строительство элитного монолитно-кирпичного 18-ти этажного 5-ти секционного Жилого комплекса <strong>«Никольская Панорама»</strong> (поз. 8,9).

                    <p><strong>Жилой комплекс «Никольская Панорама»</strong>  расположен в центральной части г. Солнечногорск, что обеспечивает удачный и легкий подъезд и парковку личного автотранспорта. Если же вы пользуетесь общественным автотранспортом, в таком случае Вам гарантирована шаговая доступность до электричек и автобусов!</p>

                    <p>Инфраструктура комплекса: три детских сада, две школы, новая детская поликлиника, магазины, салоны красоты. В шаговой доступности развлекательный комплекс, уютные рестораны, современный кинотеатр, фитнес клуб, озеро «Сенеж». Комплекс предусматривает комплексное благоустройство придомовой территории с детскими и спортивными площадками, прогулочными дорожками, аллеями, цветниками.</p>
                </div>
            </div>
            <div style="clear:both;"></div>
            <div class="homesnik">
                <P><h3>Технические характеристики дома по адресу г. Солнечногорск, ул. Баранова, поз. 8-9:</h3></P>
                <a href="/uploads/project/nikolskaya-panorama-2.png" class="fbox"><img src="/uploads/project/nikolskaya-panorama-2.png" alt="Никольская панорама" title="Никольская панорама" style="height: 225px;"></a>
                <a href="/uploads/project/nikolskaya-panorama-3.png" class="fbox"><img src="/uploads/project/nikolskaya-panorama-3.png" alt="Никольская панорама" title="Никольская панорама" style="height: 225px;"></a>
                <a href="/uploads/project/nikolskaya-panorama-4.png" class="fbox"><img src="/uploads/project/nikolskaya-panorama-4.png" alt="Никольская панорама" title="Никольская панорама" style="height: 225px;"></a>
            </div>

            <ul style="margin-top: 0px; position: inherit;">
                <li> <strong>Общая площадь квартир:</strong> 21006 кв.м </li></br>
                <li> <strong>Количество квартир:</strong> 336</li></br>
                <li> <strong>Общая площадь нежилых помещений (цокольный, первый этаж):</strong> 2245,4 кв.м </li></br>
                <li> <strong>Бесшумные лифты (фирма Otis) в подъезде:</strong> 2 на 400 и 1000 кг </li></br>
                <li> <strong>Высота потолков:</strong> 3 м </li></br>
                <li> <strong>Окна:</strong> пластиковые с двойным стеклопакетом </li></br>
                <li> <strong>Лоджии: </strong>пластиковые раздвижные с одинарным стеклопакетом </li>
            </ul>

            <div class="plans" style="margin-top: 52px;">
                <h3>Планировки квартир:</h3>
                <a href="/uploads/project/b-1.jpg" class="fbox" rel="chem"><img src="/uploads/project/b-1.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/b-2-59.jpg" class="fbox" rel="chem"><img src="/uploads/project/b-2-59.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/b-2-63.jpg" class="fbox" rel="chem"><img src="/uploads/project/b-2-63.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/b-2-72.jpg" class="fbox" rel="chem"><img src="/uploads/project/b-2-72.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/b-2-76.jpg" class="fbox" rel="chem"><img src="/uploads/project/b-2-76.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/b-3.png" class="fbox" rel="chem"><img src="/uploads/project/b-3.png" style="height: 100px;"></a>
            </div>
            <div style="margin-top: 0; position: inherit;">
              <h1><a href="/baranova.pdf">Смотреть проектную декларацию</a></h1>
            </div>
            <div style="width: 900px">
                <a href="/uploads/project/docpr1.jpg" class="fbox"><img src="/uploads/project/docpr1.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr2.jpg" class="fbox"><img src="/uploads/project/docpr2.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr3.jpg" class="fbox"><img src="/uploads/project/docpr3.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr4.jpg" class="fbox"><img src="/uploads/project/docpr4.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr5.jpg" class="fbox"><img src="/uploads/project/docpr5.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr6.jpg" class="fbox"><img src="/uploads/project/docpr6.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr7.jpg" class="fbox"><img src="/uploads/project/docpr7.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr8.jpeg" class="fbox"><img src="/uploads/project/docpr8.jpeg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
                <a href="/uploads/project/docpr9.jpg" class="fbox"><img src="/uploads/project/docpr9.jpg" style="width:auto!important; height: 150px; margin: 2px!important;"></a>
            </div>
            <div class="plans" style="margin-top: 80px; width: 900px;  margin-left: 20px;">
                <h3 style="margin-left: 0px;">Процесс строительства желищного комплекса:</h3>
                <a href="/uploads/project/1.jpg" class="fbox" rel="chem"><img src="/uploads/project/1.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/2.jpg" class="fbox" rel="chem"><img src="/uploads/project/2.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/3.jpg" class="fbox" rel="chem"><img src="/uploads/project/3.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/4.jpg" class="fbox" rel="chem"><img src="/uploads/project/4.jpg" style="height: 100px;"></a>

                <a href="/uploads/project/5.jpg" class="fbox" rel="chem"><img src="/uploads/project/5.jpg" style="height: 100px;" ></a>
                <a href="/uploads/project/6.jpg" class="fbox" rel="chem"><img src="/uploads/project/6.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/7.jpg" class="fbox" rel="chem"><img src="/uploads/project/7.jpg" style="height: 100px;"></a>
                <a href="/uploads/project/8.jpg" class="fbox" rel="chem"><img src="/uploads/project/8.jpg" style="height: 100px;"></a>
            </div>




            <h3 style="margin-top: 61px;">Схема расположения ЖК "Никольская Панорама":</h3>
            <!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (начало) -->
            <div id="ymaps-map-id_13482251488994883828" style="width: 900px; height: 350px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);"></div>
            <div style="width: 900px; text-align: right; box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); margin-bottom: 10px;"></div>
            <script type="text/javascript">function fid_13482251488994883828(ymaps) {var map = new ymaps.Map("ymaps-map-id_13482251488994883828", {center: [36.98548119049063, 56.18535124030482], zoom: 15, type: "yandex#map"});map.controls.add("zoomControl").add("mapTools").add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));map.geoObjects.add(new ymaps.Placemark([36.984228830687925, 56.18509852930169], {balloonContent: "Никольская панорама"}, {preset: "twirl#lightblueDotIcon"}));};</script>
            <script type="text/javascript" src="http://api-maps.yandex.ru/2.0/?coordorder=longlat&load=package.full&wizard=constructor&lang=ru-RU&onload=fid_13482251488994883828"></script>
            <!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->
        </div>
    </div>

    <p style="padding-left:383px; padding-top: 15px; float:left;">
        <a href="/"> <img src="tomain.png" border="0" onmouseover="this.src='tomain_hover.png'" onmouseout="this.src='tomain.png'" /></a> </div>
</p>