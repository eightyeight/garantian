<div id="update">
    <div class="row" >
        <ul class="marginal" style="display: -webkit-box;">
            <li><?php echo CHtml::link('КВАРТИРЫ', array('catalog/index', 'type' => 1));?></li>
            <li><?php echo CHtml::link('ДОМА',  array('catalog/index', 'type' => 2));?></li>
            <li><?php echo CHtml::link('ЗЕМЛЯ',  array('catalog/index', 'type' => 4));?></li>
        </ul>

    </div>



    <div class="row">

        <div class="col-lg-8">
            <div class="header_search">
                <a href="/">Сортировать по цене | </a>
                <a href="catalog.html">Сортировать по дате | </a>
                <a href="services.html">Сортировать по расстоянию | </a>
            </div>
        </div>


        <div class="col-lg-4">
            <p class="text-center"><strong>ПОИСК ПО КВАРТИРАМ</strong></p>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'_view',
                'pager' => array(
                    'firstPageLabel'=>'',
                    'prevPageLabel'=>'&laquo',
                    'nextPageLabel'=>'&raquo',
                    'lastPageLabel'=>'',
                    'maxButtonCount'=>'10',
                    'header'=>'',
                    'cssFile'=>false,
                ),

            ));
            ?>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="selector">

        </div>
    </div>
</div>