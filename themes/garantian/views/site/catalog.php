<div class="row">
    <ul class="marginal" style="display: -webkit-box;">
        <li class="active"><a href="#">КВАРТИРЫ</a></li>
        <li><a href="#">ДОМА</a></li>
        <li><a href="#">ЗЕМЛЯ</a></li>
    </ul>

</div>

<div class="row">

    <div class="col-lg-8">
        <div class="header_search">
            <a href="/">Сортировать по цене | </a>
            <a href="catalog.html">Сортировать по дате | </a>
            <a href="services.html">Сортировать по расстоянию | </a>
        </div>
    </div>


    <div class="col-lg-4">
        <p class="text-center"><strong>ПОИСК ПО КВАРТИРАМ</strong></p>
    </div>
</div>



<div class="row">
<div class="col-lg-8">
    <div class="row border">
        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="text-center">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-4" >
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>



    </div>

    <div class="row border">

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="row border">

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="list-style-type: none">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="thumbnail">
                <img src="img/home.png" alt="">
                <div class="caption">
                    <ul style="unlisted">
                        <li>Площадь: 152</li>
                        <li>Комнат: 2</li>
                        <li>Этаж: 5/12</li>
                        <li>Цена: 5 000 000</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="col-lg-4" style="padding-left: 3em">
    <div class="row_search">
        <div class="body_search">
            Количество комнат
        </div>

        <p>
            <?php Yii::app()->controller->renderPartial('//site/_search_field_rooms'); ?>
        </p>
    </div>

    <div class="row_search">
        <div class="body_search">
            Общая площадь
        </div>
        <?php Yii::app()->controller->renderPartial('//site/_search_field_price'); ?>
        от <input type="number" class="form-control1"> -до
        <input type="number" class="form-control1"> кв.м

    </div>

    <div class="row_search">
        <div class="body_search">
            Балкон \ Лоджия
        </div>

        <p><input type="checkbox" id="inlineCheckbox1" value="option1"> Балкон
            </label></p>
        <p><label class="checkbox-inline">
                <input type="checkbox" id="inlineCheckbox2" value="option2"> Лоджия
            </label></p>
    </div>

    <div class="row_search">
        <div class="body_search">
            Цена
        </div>
        от <input type="number" class="form-control1"> -до
        <input type="number" class="form-control1"> млн.р
    </div>

    <div class="row_search">
        <div class="body_search">
            Площадь кухни
        </div>
        от <input type="number" class="form-control1"> -до
        <input type="number" class="form-control1"> кв.м
    </div>

    <div class="row_search">

        <div class="body_search">Диапазон этажей </div>
        от <input type="number" class="form-control1"> -до
        <input type="number" class="form-control1"> этажа


        <p><input type="checkbox" id="inlineCheckbox1" value="option1"> Не первый
            </label></p>
        <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckbox2" value="option2">Не последний
        </label>


    </div>

    <div class="under" style=""><button type="button" class="btn btn-lg btn-warning">НАЙТИ</button></div>


</div>


</div>

<div class="col-lg-8">
    <div class="selector">
        <ul class="text-center">
            <li><a href="#">«</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">»</a></li>
        </ul>
    </div>
</div>
