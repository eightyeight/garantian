
<div class="col-xs-4">
    <div class="thumbnail">
        <?php echo $data->returnMainThumbForGrid($data);?>
        <div class="caption">
            <ul style="text-center">
                <li>Площадь: <?php echo $data->square?> м <sup>2</sup></li>
                <li>Комнат: <?php echo $data->num_of_rooms?></li>
                <li>Этаж: <?php echo $data->floor?></li>
                <li>Цена: <?php echo $data->price?> руб.</li>
            </ul>
        </div>
    </div>
</div>