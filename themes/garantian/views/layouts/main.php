<!DOCTYPE html>
<?php

$cs = Yii::app()->clientScript;
$baseUrl = Yii::app()->baseUrl;
$baseThemeUrl = Yii::app()->theme->baseUrl;
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language;?>" lang="<?php echo Yii::app()->language;?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title><?php echo CHtml::encode($this->seoTitle ? $this->seoTitle : $this->pageTitle); ?></title>
	<meta name="description" content="<?php echo CHtml::encode($this->seoDescription ? $this->seoDescription : $this->pageDescription); ?>" />
	<meta name="keywords" content="<?php echo CHtml::encode($this->seoKeywords ? $this->seoKeywords : $this->pageKeywords); ?>" />








	<link rel="icon" href="<?php echo $baseUrl; ?>/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $baseUrl; ?>/favicon.ico" type="image/x-icon" />


	<?php
	$cs->registerCoreScript('jquery');
	$cs->registerCoreScript('jquery.ui');
	$cs->registerCoreScript('rating');
	$cs->registerCssFile($cs->getCoreScriptUrl().'/rating/jquery.rating.css');
	$cs->registerCssFile($baseThemeUrl . '/css/ui/jquery-ui.multiselect.css');
	$cs->registerCssFile($baseThemeUrl . '/css/redmond/jquery-ui-1.7.1.custom.css');
	$cs->registerCssFile($baseThemeUrl . '/css/ui.slider.extras.css');
	$cs->registerScriptFile($baseThemeUrl . '/js/jquery.multiselect.min.js');
	$cs->registerCssFile($baseThemeUrl . '/css/ui/jquery-ui.multiselect.css');
	$cs->registerScriptFile($baseThemeUrl . '/js/jquery.dropdownPlain.js', CClientScript::POS_HEAD);
	$cs->registerScriptFile($baseThemeUrl . '/js/common.js', CClientScript::POS_HEAD);
	$cs->registerScriptFile($baseThemeUrl . '/js/habra_alert.js', CClientScript::POS_END);
	$cs->registerScriptFile($baseThemeUrl . '/js/jquery.cookie.js', CClientScript::POS_END);
	$cs->registerScriptFile($baseThemeUrl . '/js/scrollto.js', CClientScript::POS_END);
	$cs->registerCssFile($baseThemeUrl.'/css/form.css', 'screen, projection');

	// superfish menu
	$cs->registerCssFile($baseThemeUrl.'/js/superfish/css/superfish.css', 'screen');
	$cs->registerCssFile($baseThemeUrl.'/js/superfish/css/superfish-vertical.css', 'screen');
	$cs->registerScriptFile($baseThemeUrl.'/js/superfish/js/hoverIntent.js', CClientScript::POS_HEAD);
	$cs->registerScriptFile($baseThemeUrl.'/js/superfish/js/superfish.js', CClientScript::POS_HEAD);

	$cs->registerScript('initizlize-superfish-menu', '
			$("#sf-menu-id").superfish( {delay: 100, autoArrows: false, dropShadows: false, pathClass: "overideThisToUse", speed: "fast" });
		', CClientScript::POS_READY);

	if(param('useYandexMap') == 1){
		$cs->registerScriptFile('http://api-maps.yandex.ru/2.0/?load=package.standard,package.clusters&coordorder=longlat&lang='.CustomYMap::getLangForMap(), CClientScript::POS_END);
	}
	elseif (param('useGoogleMap') == 1){
		//$cs->registerScriptFile('https://maps.google.com/maps/api/js??v=3.5&sensor=false&language='.Yii::app()->language.'', CClientScript::POS_END);
		//$cs->registerScriptFile('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js', CClientScript::POS_END);
	}
	elseif (param('useOSMMap') == 1){
		//$cs->registerScriptFile('http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.js', CClientScript::POS_END);
		//$cs->registerCssFile('http://cdn.leafletjs.com/leaflet-0.7.2/leaflet.css');

		$cs->registerScriptFile($baseThemeUrl . '/js/leaflet/leaflet-0.7.2/leaflet.js', CClientScript::POS_HEAD);
		$cs->registerCssFile($baseThemeUrl . '/js/leaflet/leaflet-0.7.2/leaflet.css');

		$cs->registerScriptFile($baseThemeUrl . '/js/leaflet/leaflet-0.7.2/dist/leaflet.markercluster-src.js', CClientScript::POS_HEAD);
		$cs->registerCssFile($baseThemeUrl . '/js/leaflet/leaflet-0.7.2/dist/MarkerCluster.css');
		$cs->registerCssFile($baseThemeUrl . '/js/leaflet/leaflet-0.7.2/dist/MarkerCluster.Default.css');
	}

	if(Yii::app()->user->getState('isAdmin')){
		?><link rel="stylesheet" type="text/css" href="<?php echo $baseThemeUrl; ?>/css/tooltip/tipTip.css" /><?php
	}
	?>

    <script src="<?php echo $baseThemeUrl; ?>/js/bootstrap.js"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $baseThemeUrl; ?>/css/bootstrap.css" rel="stylesheet"  />
    <link rel="stylesheet" type="text/css" href="<?php echo $baseThemeUrl; ?>/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo $baseThemeUrl; ?>/css/styles.css" rel="stylesheet" />
    <script>
        $('#myModal').modal('show');
    </script>
</head>

<body>

<div class="container">
<div class="page-header">
    <div class="row">
        <div class="col-lg-5">
            <img src="<?php echo $baseThemeUrl; ?>/images/logo.png" alt="Альтернативный текст" style="height: 89px;
                        position: absolute;">
            <p style="color: #027e08;
                        margin: 0;
                        position: absolute;
                        top: 86px;">
                <strong>СОЛНЕЧНОГОРСКОЕ АГЕНТСТВО НЕДВИЖИМОСТИ</strong>
            </p>
        </div>
        <div class="col-lg-4 phone" >
            <p class="text-right"><strong>+7 (499) 707-75-85</strong></p>
            <p class="text-right"><strong>+7 (903) 216-89-98</strong></p>
            <p class="text-right"><strong>+7 (916) 270-78-23</strong></p>
        </div>
        <?php $this->widget('application.components.callform.CallFormWidget'); ?>
    </div>

            <?php $this->widget('application.components.searchform.SearchFormWidget'); ?>


    <div class='row'>

        <div class="navbar-header">
            <?php
            $this->widget('zii.widgets.CMenu', array(
                'items'=>array(
                    array('label'=>'ГЛАВНАЯ', 'url'=> ''. Yii::app()->request->hostInfo .'/site/index'),
                    array('label'=>'КАТАЛОГ ПРЕДЛОЖЕНИЙ', 'url'=>''. Yii::app()->request->hostInfo .'/catalog/index'),
                    array('label'=>'ЮРИДИЧЕСКИЙ КАБИНЕТ', 'url'=>''. Yii::app()->request->hostInfo .'/jurist/index'),
                    array('label'=>'КОНТАКТЫ', 'url'=>''. Yii::app()->request->hostInfo .'/contact/index'),
                    array('label'=>'О КОМПАНИИ', 'url'=>''. Yii::app()->request->hostInfo .'/company/index'),
                ),
                'htmlOptions' => array(
                    'class' => 'nav nav-justified',
                    'style' => 'margin-top: 1em; color: green;'
                ),
                'linkLabelWrapper' => 'strong'
            ));
            ?>
        </div>

    </div>
</div>

<?php echo $content?>

    <div class="footer">
        <div class="row">
            <div class="col-md-3">
                <div class="share42init"></div>
            </div>
            <?php
            $this->widget('zii.widgets.CMenu', array(
                'items'=>array(
                    array('label'=>'Главная', 'url'=> ''. Yii::app()->request->hostInfo .'/site/index'),
                    array('label'=>'Каталог предложений', 'url'=>''. Yii::app()->request->hostInfo .'/catalog/index'),
                    array('label'=>'Юридический кабинет', 'url'=>''. Yii::app()->request->hostInfo .'/assist/index'),
                    array('label'=>'Контакты', 'url'=>''. Yii::app()->request->hostInfo .'/contact/index'),
                    array('label'=>'О компании', 'url'=>''. Yii::app()->request->hostInfo .'/about/index'),
                    array('label'=>'garantian.ru', 'url'=> Yii::app()->request->hostInfo),
                ),
                'htmlOptions' => array(
                    'class' => '',
                    'style' => 'margin-top: 1em; color: green;'
                ),
                'itemCssClass' => 'col-md-2',

            ));
            ?>
        </div>
        </div>
    </div>
</body>
</html>