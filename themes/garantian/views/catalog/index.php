<div id="update">
    <div class="row" >
        <ul class="marginal" style="display: -webkit-box;">
            <li><?php echo CHtml::link('КВАРТИРЫ', array('catalog/index', 'type' => 1));?></li>
            <li><?php echo CHtml::link('ДОМА',  array('catalog/index', 'type' => 2));?></li>
            <li><?php echo CHtml::link('ЗЕМЛЯ',  array('catalog/index', 'type' => 4));?></li>
        </ul>

    </div>



    <div class="row">

<!--        <div class="col-lg-8">-->
<!--            <div class="header_search">-->
<!--                <a href="/">Сортировать по цене | </a>-->
<!--                <a href="catalog.html">Сортировать по дате | </a>-->
<!--                <a href="services.html">Сортировать по расстоянию | </a>-->
<!--            </div>-->
<!--        </div>-->




        <div class="col-lg-8">
            <?php
                $this->widget('application.components.catalogListView.CatalogListView', array(
                'dataProvider'   =>$dataProvider,
                'enableSorting'  => true,
                'sortableAttributes' => array('price', 'date_created'),
                'itemView'=>'_view',
                    'pager' => array(
                        'firstPageLabel' => '',
                        'prevPageLabel'  => '&laquo',
                        'nextPageLabel'  => '&raquo',
                        'lastPageLabel'  => '',
                        'maxButtonCount' => '10',
                        'header'         => '',
                        'cssFile'        => false,
                    ),

                ));
            ?>
        </div>

        <div class="col-lg-4" style="padding-left: 3em">
            <?php $this->widget('application.components.filterform.FilterFormWidget', array(
               'model' => $model
            ));?>
        </div>


    </div>

    <div class="col-lg-8">
        <div class="selector">

        </div>
    </div>
</div>