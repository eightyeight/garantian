<div class="row">

    <div class="col-lg-4 urservice"><h3 style="color: green">УСЛУГИ:</h3>

        <ul class="nav" role="tablist" style="margin-left: -4em;">
            <?php foreach($data as $service):?>
                <li <?php echo $service->id == 1 ? 'class="active"' : '';?>><a href="#<?php echo $service->id;?>" role="tab" data-toggle="tab"><?php echo $service->title;?></a></li>
            <?php endforeach;?>
        </ul>

        <!-- Tab panes -->


    </div>
    <div class="col-lg-8">
        <div class="tab-content" style="font-weight: normal">
            <?php foreach($data as $service):?>
                <div class="tab-pane <?php echo $service->id == 1 ? 'active' : '';?>" id="<?php echo $service->id;?>">
                    <h4><?php echo $service->title;?></h4>
                    <p><?php echo $service->content;?></p>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
