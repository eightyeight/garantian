

<div class='div-pdf-fix'>
	<?php
		echo '<div class="floatleft printicon">';




		echo CHtml::link('<img src="'.Yii::app()->theme->baseUrl.'/images/design/print.png"
				alt="'.tc('Print version').'" title="'.tc('Print version').'"  />',
			$model->getUrl().'?printable=1', array('target' => '_blank'));


		$editUrl = $model->getEditUrl();

		if($editUrl){
			echo CHtml::link('<img src="'.Yii::app()->theme->baseUrl.'/images/design/edit.png"
				alt="'.tt('Update apartment').'" title="'.tt('Update apartment').'"  />',
				$editUrl);
		}
		echo '</div>';
	?>

	<div class="floatleft-title">
		<div>
			<div class="div-title">
				<h1 class="h1-ap-title"><?php echo CHtml::encode($model->getStrByLang('title')); ?></h1>
			</div>

		</div>
		<div class="clear"></div>

	</div>
</div>
<div class="clear"></div>
<?php
	// show ad
	$this->renderPartial('_view', array(
		'data'=>$model,
	));
?>
