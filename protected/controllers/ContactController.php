<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 06.10.14
 * Time: 12:11
 */

class ContactController extends Controller {


    public $cityActive;
    public $newFields;

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'MathCCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('viewreferences'),
                'expression' => 'Yii::app()->user->getState("isAdmin")',
            ),
        );
    }

    public function init() {
        parent::init();
        $this->cityActive = SearchForm::cityInit();
    }

    public function actionIndex(){

        $this->render('index');

    }
} 