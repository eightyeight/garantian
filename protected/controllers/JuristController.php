<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 06.10.14
 * Time: 12:27
 */
Yii::import('application.modules.assist.models.Assist');

class JuristController extends Controller {

    public $cityActive;
    public $newFields;

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'MathCCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('viewreferences'),
                'expression' => 'Yii::app()->user->getState("isAdmin")',
            ),
        );
    }

    public function init() {
        parent::init();
        $this->cityActive = SearchForm::cityInit();
    }

    public function actionIndex(){
        $data = Assist::model()->byid()->findAll();
        $this->render('index', array('data' => $data));
    }

    public function actionView($id){
        $assist = Assist::model()->findByPk($id);
        $this->render('view', array('data' => $assist));
    }

} 