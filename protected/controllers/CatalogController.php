<?php
/**
 * Php class file Catalog Controller.
 */

class CatalogController extends Controller {

    public $cityActive;
    public $newFields;

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'MathCCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('viewreferences'),
                'expression' => 'Yii::app()->user->getState("isAdmin")',
            ),
        );
    }

    public function init() {
        parent::init();
        $this->cityActive = SearchForm::cityInit();
    }

    /**
     * Index page action.
     */

    public function actionIndex( $type = 1){

        $model = new FilterForm;

        if(Yii::app()->request->isAjaxRequest){

            if(isset($_POST['FilterForm'])){

                $model->attributes = $_POST['FilterForm'];

                if($model->validate()){

                    $criteria = new CDbCriteria();

                    $criteria->with = array('reference_value');

                    $criteria->together=true;

                    $criteria->group = 't.id';

                    if(isset($_POST['type']) && $_POST['type'] > 0 ){
                        $type = $_POST['type'];
                        $criteria->addCondition('obj_type_id = :type' );
                        $criteria->params = array(':type' => (int)$type);
                    }

                    if($model->rooms){
                        $criteria->addInCondition('num_of_rooms', $model->rooms);
                    }

                    if($model->square_from && $model->square_to){
                        $criteria->addBetweenCondition('square', $model->square_from, $model->square_to);
                    }
                    elseif($model->square_from && !$model->square_to){
                        $criteria->addCondition('square > ' . $model->square_from);
                    }
                    elseif(!$model->square_from && $model->square_to){
                        $criteria->addCondition('square < ' . $model->square_to);
                    }

                    if($model->price_from && $model->price_to){
                        $criteria->addBetweenCondition('price', $model->price_from, $model->price_to);
                    }
                    elseif($model->price_from && !$model->price_to){
                        $criteria->addCondition('price > ' . $model->price_from);
                    }
                    elseif(!$model->price_from && $model->price_to){
                        $criteria->addCondition('price < ' . $model->price_to);
                    }

                    if($model->kitchen_min && $model->kitchen_max){
                        $criteria->addBetweenCondition('price', $model->kitchen_min, $model->kitchen_max);
                    }
                    elseif($model->kitchen_min && !$model->kitchen_max){
                        $criteria->addCondition('price > ' . $model->kitchen_min);
                    }
                    elseif(!$model->kitchen_min && $model->kitchen_max){
                        $criteria->addCondition('price < ' . $model->kitchen_max);
                    }

                    if($model->floor_min && $model->floor_max){
                        $criteria->addBetweenCondition('floor', $model->floor_min, $model->floor_max);
                    }
                    elseif($model->floor_min && !$model->floor_max){
                        $criteria->addCondition('floor > ' . $model->floor_max);
                    }
                    elseif(!$model->floor_min && $model->floor_max){
                        $criteria->addCondition('floor < ' . $model->floor_max);
                    }

                    if($model->balcony){
                        $criteria->addCondition('reference_value.title_en = "Балкон"');

                    }

                    //OOH, SHII...
                    //May be good idea would be create a class, that encapsulates all this criteria params.

                    if($model->electricity[0] == 1){
                        $criteria->addCondition('reference_value.title_en = "Есть"');
                        $criteria->addCondition('reference_value.reference_category_id = 13');
                    }

                    if($model->gas[0] == 1){
                        $criteria->addCondition('reference_value.title_en = "Есть"');
                        $criteria->addCondition('reference_value.reference_category_id = 14');
                    }

                    if($model->loggia){
                        $criteria->addCondition('reference_value.title_en = "Лоджия"');
                    }

                    if($model->floor_first){
                        $criteria->addCondition('floor != 1');
                    }

                    if($model->floor_last){
                        $criteria->addCondition('floor != floor_total');
                    }



                    $dataProvider=new CActiveDataProvider(Apartment::model()->active(), array(
                        'criteria'=> $criteria,
                        'pagination'=>array(
                            'pageSize'=> 9,
                            'pageVar'=>'page'
                        ),
                        'sort'=>array(
                            'attributes'=>array(
                                'price'=>array(
                                    'asc'=>'price ASC',
                                    'desc'=>'price DESC',
                                    'default'=>'desc',
                                ),
                                'date_created'=>array(
                                    'asc'=>'date_created ASC',
                                    'desc'=>'date_created DESC',
                                    'default'=>'desc',
                                )
                            ),
                        ),
                    ));

                        $this->renderPartial('index', array('model' => $model, 'dataProvider' => $dataProvider));
                }
            }
        }
        else{

            if( !isset($type) || !$type)
                $type = Yii::app()->request->getQuery('type');

            $dataProvider=new CActiveDataProvider(Apartment::model()->active(), array(
                'criteria'=> array(
                    'condition'=>'obj_type_id = '. $type,
                ),
                'pagination'=>array(
                    'pageSize'=> 9,
                    'pageVar'=>'page'
                ),
                'sort'=>array(
                    'attributes'=>array(
                        'price'=>array(
                            'asc'=>'price ASC',
                            'desc'=>'price DESC',
                            'default'=>'desc',
                        ),
                        'date_created'=>array(
                            'asc'=>'date_created ASC',
                            'desc'=>'date_created DESC',
                            'default'=>'desc',
                        )
                    ),
                ),
            ));

            $this->render('index', array('model' => $model, 'dataProvider' => $dataProvider));
        }
    }

} 