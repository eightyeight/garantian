<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 03.10.14
 * Time: 15:00
 */

class FilterFormWidget extends CWidget{

    public $model;

    public function init(){

        parent::init();

        $cs = Yii::app()->getClientScript();

        $cs->registerScript("filter-form","
            function filterSubmit(){

                var data = $('#filter').serialize();
                console.log(data);
                $.ajax({
                   type: 'POST',
                   url: '" . Yii::app()->createUrl('catalog/index'). "',
                   data: 'YII_CSRF_TOKEN=" . Yii::app()->request->csrfToken . "&' + data ,
                   success: function(data){
                     $('#update').html(data);
                   }
                 });
            }
        ", CClientScript::POS_HEAD);
    }

    public function run(){

        if(Yii::app()->request->getParam('type') == 1)

            $this->render('flats', array('model' => $this->model));

        elseif(Yii::app()->request->getParam('type') == 2)

            $this->render('houses', array('model' => $this->model));

        elseif(Yii::app()->request->getParam('type') == 4)

            $this->render('land', array('model' => $this->model));

        else
            $this->render('flats', array('model' => $this->model));

    }

} 