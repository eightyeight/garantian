<p class="text-center"><strong>ПОИСК ПО КВАРТИРАМ</strong></p>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'filter',

)); ?>
    <input type="hidden" name="type" value="<?php echo Yii::app()->request->getParam('type', 1);?>">
    <div class="row_search">
        <div class="body_search">
            Количество комнат
        </div>

        <p>
            <?php
            $data = array(1 => '1', 2 => '2', 3 => '3', 4 =>'4+');
            echo $form->checkBoxList($model, 'rooms', $data, array(
                'separator' => '',
                'class' => 'checkbox-inline',
                'labelOptions' => array('class'=>'checkbox-inline'),
            ) );?>

        </p>
    </div>

    <div class="row_search">
        <div class="body_search">
            Общая площадь
        </div>
        от <?php  echo $form->textField($model,
            'square_from',
            array(
                'class' => 'form-control1 filter-input',

            )
        );?>
        -до <?php  echo $form->textField($model,
            'square_to',
            array(
                'class' => 'form-control1 filter-input',

            )
        );?> кв.м

    </div>

    <div class="row_search">
        <div class="body_search">
            Балкон \ Лоджия
        </div>

        <p>

            <?php echo $form->checkBox($model, 'balcony')?>
            Балкон

        </p>
        <p>

            <?php echo $form->checkBox($model,'loggia')?>
            Лоджия

        </p>
    </div>

    <div class="row_search">
        <div class="body_search">
            Цена
        </div>
        от <?php echo $form->textField($model, 'price_from', array('class' => 'form-control1 filter-input'));?>
        -до <?php echo $form->textField($model, 'price_to', array('class' => 'form-control1 filter-input'));?>
        млн.р
    </div>

    <div class="row_search">
        <div class="body_search">
            Площадь кухни
        </div>
        от <?php echo $form->textField($model, 'kitchen_min', array('class' => 'form-control1 filter-input'));?>
        -до <?php echo $form->textField($model, 'kitchen_max', array('class' => 'form-control1 filter-input'));?>
        кв.м
    </div>

    <div class="row_search">

        <div class="body_search">Диапазон этажей </div>
        от <?php echo $form->textField($model, 'floor_min', array('class' => 'form-control1 filter-input'));?>
        -до <?php echo $form->textField($model, 'floor_max', array('class' => 'form-control1 filter-input'));?>
        этажа

        <p>
            <?php echo $form->checkBox($model, 'floor_first')?> Не первый
        </p>

        <p>
            <?php echo $form->checkBox($model, 'floor_last')?> Не последний
        </p>


    </div>
    <div class="under" style="">
        <?php echo CHtml::button('Найти',
            array(
                'class' => 'btn btn-lg btn-warning',
                'id' => 'filter-submit',
                "onclick" => 'filterSubmit(); return false;'
            )
        );?>

    </div>
<?php $this->endWidget(); ?>