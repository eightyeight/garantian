<p class="text-center"><strong>ПОИСК ПО УЧАСТКАМ</strong></p>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'filter',

)); ?>
<input type="hidden" name="type" value="<?php echo Yii::app()->request->getParam('type', 1);?>">


<div class="row_search">
    <div class="body_search">
        Цена
    </div>
    от <?php echo $form->textField($model, 'price_from', array('class' => 'form-control1 filter-input'));?>
    -до <?php echo $form->textField($model, 'price_to', array('class' => 'form-control1 filter-input'));?>
    млн.р
</div>

<div class="row_search">
    <div class="body_search">
        Площадь Участка
    </div>
    от <?php  echo $form->textField($model, 'square_from', array('class' => 'form-control1 filter-input'));?>
    -до <?php  echo $form->textField($model, 'square_to', array('class' => 'form-control1 filter-input'));?> кв.м

</div>



<div class="row_search">
    <div class="body_search">
        Площадь Участка
    </div>
    от <?php  echo $form->textField($model, 'area_min', array('class' => 'form-control1 filter-input'));?>
    -до <?php  echo $form->textField($model, 'area_max', array('class' => 'form-control1 filter-input'));?> соток

</div>

<div class="row_search">
    <div class="body_search">
        Элекстричество
    </div>

    <p>
        <?php
        $data = array(1 => 'Есть', 0 => 'Нет');
        echo $form->checkBoxList($model, 'electricity', $data, array(
            'separator' => '',
            'class' => 'checkbox-inline',
            'labelOptions' => array('class'=>'checkbox-inline')
        ) );?>

    </p>
</div>
<div class="row_search">
    <div class="body_search">
        Газ
    </div>

    <p>
        <?php
        $data = array(1 => 'Есть', 0 => 'Нет');
        echo $form->checkBoxList($model, 'gas', $data, array(
            'separator' => '',
            'class' => 'checkbox-inline',
            'labelOptions' => array('class'=>'checkbox-inline')
        ) );?>

    </p>
</div>

<div class="under" style="">
    <?php echo CHtml::button('Найти',
        array(
            'class' => 'btn btn-lg btn-warning',
            'id' => 'filter-submit',
            "onclick" => 'filterSubmit(); return false;'
        )
    );?>

</div>
<?php $this->endWidget(); ?>