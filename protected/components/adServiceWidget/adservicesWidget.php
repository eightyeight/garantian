<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 19.02.15
 * Time: 15:00
 */
Yii::import('application.modules.adservice.models.AdService');
class adservicesWidget extends CWidget {

    public function init(){
        parent::init();
    }


    public function run(){

        $data = AdService::model()->sorted()->findAll();

        foreach($data as $service){
            echo '<div class="col-lg-4">
                    <a href="'.$service->link.'">
                         <img class="img-circle" data-src="holder.js/140x140" alt="140x140" src="/uploads/iconsmap/'.$service->image.'">
                    </a>
                <h4 class="text-center">'.$service->title.'</h4>
            </div><!-- /.col-lg-4 -->';
        }
    }

}