<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 20.10.14
 * Time: 16:11
 */

class LatestOffersWidget extends CWidget {

    public function init(){
        parent::init();
    }


    public function run(){

        $criteria = new CDbCriteria();


        $criteria->select ='title_en';
        $criteria->order  ='date_created DESC';
        $criteria->limit  = 2;

        $data = Apartment::model()->active()->findAll($criteria);

        $this->render('index', array('data' => $data));

    }

} 