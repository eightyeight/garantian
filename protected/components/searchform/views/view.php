<!--<form class="navbar-form navbar-right" role="search">-->


    <?php $form = $this->beginWidget('CActiveForm',
        array(
            'action' => Yii::app()->createUrl('site/search'),
            'method' => 'post',
            'htmlOptions' => array(
                'class' => 'navbar-form navbar-right'
            )
        )
    ); ?>
    <div class="form-group">
    <?php echo CHtml::beginForm(); ?>

        <?php  echo $form->textField($search, 'string', array(
            'class' => 'form-control1',
            'size'  => '40',
            'placeholder' => 'Поиск по сайту'
        ));?>
        <button type="submit" class="btn btn-default">
            <span class="glyphicon glyphicon-search">
        </button>
    </div>
    <?php $this->endWidget(); ?>


