<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 06.10.14
 * Time: 14:41
 */

class SearchFormWidget extends CWidget{


    public function init(){
        parent::init();
    }

    public function run(){

        $search = new SearchMainForm();

        $this->render('view', array('search' => $search));

    }
} 