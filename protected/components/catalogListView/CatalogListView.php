<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 09.10.14
 * Time: 12:39
 */

Yii::import('zii.widgets.CListView');


class CatalogListView extends CListView{

    public $template="{sorter}\n{items}\n
    <div class='clearfix'></div>
    {pager}\n";


    public function renderSorter()
    {
        if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes))
            return;
        echo CHtml::openTag('div',array('class'=> 'header_search'))."\n";

            $sort=$this->dataProvider->getSort();
            foreach($this->sortableAttributes as $name => $label)
            {
                if($label == 'date_created') {
                    $link_name = "<span>Сортировать по дате</span>";
                } else {
                    $link_name = "<span>Сортировать по цене</span>";
                }

                if(is_integer($name)){

                    //если производят сортировку
                    if(!empty($_GET['Apartment_sort'])) {

                        $sorter_type = $_GET['Apartment_sort'];

                        if($sorter_type == 'date_created' && $name == 1) {
                            echo $sort->link($label, $link_name.' ▼', $htmlOptions=array('class'=>'active'));

                        } elseif($sorter_type == 'date_created.desc' && $name == 1) {
                            echo $sort->link($label, $link_name.'  ▲', $htmlOptions=array('class'=>'active'));

                        } elseif($sorter_type == 'price' && $name == 0) {
                            echo $sort->link($label, ''.$link_name.'  ▼', $htmlOptions=array('class'=>'active'));

                        } elseif($sorter_type == 'price.desc' && $name == 0) {
                            echo $sort->link($label, $link_name.' ▲', $htmlOptions=array('class'=>'active'));

                        } else {
                            echo $sort->link($label, $link_name);
                        }


                    } else {

                        //sorter до сортировки
                        if($label == 'created_at') {
                            echo $sort->link($label, $link_name.' ▲', $htmlOptions=array('class'=>'active'));
                        } else {
                            echo $sort->link($label, $link_name);
                        }
                    }

                } else {
                    echo $sort->link($name,$label);
                }
            }

        echo $this->sorterFooter;
        echo CHtml::closeTag('div');
        echo CHtml::openTag('div',array('class'=> 'clearfix'))."\n";
        echo CHtml::closeTag('div');

    }

    public function renderPager()
    {
        if(!$this->enablePagination)
            return;

        $pager=array();
        $class='CLinkPager';
        if(is_string($this->pager))
            $class=$this->pager;
        elseif(is_array($this->pager))
        {
            $pager=$this->pager;
            if(isset($pager['class']))
            {
                $class=$pager['class'];
                unset($pager['class']);
            }
        }
        $pager['pages']=$this->dataProvider->getPagination();

        if($pager['pages']->getPageCount()>1)
        {
            echo '<div class="custom-pager">';
            $this->widget($class,$pager);
            echo '</div>';
        }
        else
            $this->widget($class,$pager);
    }

} 