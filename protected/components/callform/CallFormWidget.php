<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 07.10.14
 * Time: 17:24
 */



class CallFormWidget extends CWidget {

    public function init(){
        parent::init();

        $cs  = Yii::app()->getClientScript();
        $cs->registerScript('call-form',"
            function callSubmit(){

               var data = $('#call-form').serialize();

               $.ajax({
                   type: 'POST',
                   url: '" . Yii::app()->createUrl('site/call'). "',
                   data: 'YII_CSRF_TOKEN=" . Yii::app()->request->csrfToken . "&' + data ,
                   success: function(data){
                        $('#myModal').html('<h2>Ваша заявка принята.</h2>').delay(1000).fadeOut('slow');
                        $('.modal-backdrop').hide();
                        console.log(data);
                   },
                   error: function(data){
                        console.log(data);
                   }
                 });
            }
        ", CClientScript::POS_END);

    }

    public function run(){

        $model = new CallForm();

        $this->render('view', array('model' => $model));
    }


} 