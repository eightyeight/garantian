<div class="col-lg-3 call">
    <a href="#myModal" data-toggle="modal">
        <button type="button" class="btn btn-lg btn-success" id="callButton"><span class="glyphicon glyphicon-earphone" style="margin-right: 5px"></span> Заказать звонок</button>
    </a>
</div>
<?php $form = $this->beginWidget('CActiveForm',
    array(
        'id'     => 'call-form'
    )
); ?>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel" style="text-align: center;">Заказ звонка</h3>
        </div>
        <div class="modal-body" >

            <div style="padding: 10px;">
                <?php  echo $form->textField($model, 'name', array(
                    'class' => 'modal-input',
                    'placeholder' => 'Ваше Имя'
                ));?>
            </div>

            <div style="padding: 10px;">
                <?php  echo $form->textField($model, 'phone', array(
                    'class' => 'modal-input',
                    'placeholder' => 'Ваш Телефон'
                ));?>
            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-success" onclick="callSubmit(); return false;" >Отправить</button>
        </div>
    </div>
<?php $this->endWidget(); ?>