<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 21.10.14
 * Time: 10:12
 */

class CSliderWidget extends CWidget{


    public $assetsDir;

    public function init(){

        parent::init();

        $this->assetsDir = Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.components.slider.assets'));
    }

    public function run(){


        $criteria = new CDbCriteria();

        $criteria->addCondition('slider = 1');

        $data = Apartment::model()->findAll($criteria);

        $this->render('view', array('data' => $data));
    }
} 