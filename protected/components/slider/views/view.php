<div  class="carousel slide" data-ride="carousel" id="mainPageSlider">



<!--        <div class="item">-->
<!--            <img   src="img/main.png">-->
<!--            <div class="carousel-caption">-->
<!--                <h3>Квартира - студия</h3>-->
<!--                <p>4 000 000 руб.</p>-->
<!--            </div>-->
<!--        </div>-->



    <div class="carousel-inner">

        <?php foreach($data as $key => $slide):?>
            <?php
            $class= '';
            if(!$key)
                $class = 'active'?>
            <div class="item <?= $class?>">

                <?php echo $slide->returnMainThumbForSlider($slide);?>
                <div class="carousel-caption">
                    <h3><?php echo $slide->title_en?></h3>
                    <p><?php echo $slide->prettyPrice?></p>
                </div>
            </div>
        <?php endforeach;?>
    </div>

    <div class="controls-inner">

        <a class="left carousel-control"  data-slide="prev" onclick="$('#mainPageSlider').carousel('prev')">
            <img src="<?php echo $this->assetsDir; ?>/img/left.png">
        </a>

        <a class="right carousel-control"  data-slide="next" onclick="$('#mainPageSlider').carousel('next')">
            <img src="<?php echo $this->assetsDir; ?>/img/right.png">
        </a>

    </div>

</div>