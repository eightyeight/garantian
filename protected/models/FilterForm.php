<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 29.09.14
 * Time: 18:47
 */

class FilterForm extends CFormModel{

    public $rooms;

    public $square_from;

    public $square_to;

    public $balcony;

    public $loggia;

    public $price_from;

    public $price_to;

    public $kitchen_min;

    public $kitchen_max;

    public $floor_min;

    public $floor_max;

    public $floor_first;

    public $floor_last;

    public $area_min;

    public $area_max;

    public $electricity;

    public $gas;

    // For Static Instance
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('rooms, square_from, square_to, balcony, loggia, price_from, price_to, kitchen_min, kitchen_min, floor_min, floor_max, floor_first, floor_last, area_min, area_max, electricity, gas', 'safe')
        );
    }




} 