<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 07.10.14
 * Time: 12:24
 */

class CallForm extends  CFormModel{

    public $name;

    public $phone;

    public function rules(){

        return array(

            array('name, phone', 'required'),
            array('name, phone', 'safe')

        );

    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

} 