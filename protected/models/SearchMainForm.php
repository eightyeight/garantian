<?php
/**
 * Created by PhpStorm.
 * User: web
 * Date: 06.10.14
 * Time: 14:45
 */

class SearchMainForm extends CFormModel{

    public $string;

    public function rules() {

        return array(
            array('string', 'required'),
            array('string', 'safe')
        );

    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
} 