<div class="form">

<?php $form=$this->beginWidget('CustomForm', array(
	'id'=>$this->modelName.'-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?php echo Yii::t('common', 'Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo
    $form->labelEx($model, 'title', array());
    ?>
    <?php echo
    $form->textField($model, 'title', array());
    ?>
    <div class="clear"></div>

    <?php echo
    $form->labelEx($model, 'link', array());
    ?>
    <?php echo
    $form->textField($model, 'link', array());
    ?>
    <div class="clear"></div>

    <?php echo
    $form->labelEx($model, 'sort', array());
    ?>
    <?php echo
    $form->numberField($model, 'sort', array());
    ?>
    <div class="clear"></div>
	<?php
		if (!$model->isNewRecord && $model->image):
	?>
        <div class="rowold padding-bottom10 padding-top10">
	        <div class="padding-bottom10"><?php echo tt('current_icon'); ?></div>
	        <div><?php echo CHtml::image(Yii::app()->getBaseUrl().'/'.$model->iconsMapPath.'/'.$model->image); ?></div>
            <div><?php echo CHtml::link(tc('Delete'), $this->createUrl('deleteIcon', array('id' => $model->id))); ?></div>
        </div>
	<?php endif; ?>

    <div class="rowold">
		<?php echo $form->labelEx($model,'image'); ?>
        <div class="padding-bottom10">
				<span class="label label-info">
					<?php echo Yii::t('module_apartmentObjType', 'Supported file: {supportExt}.', array('{supportExt}' => $model->supportExt)).'';?>
				</span>
        </div>
		<?php echo $form->fileField($model, 'image'); ?>
		<?php echo $form->error($model,'image'); ?>
    </div>

	<div class="clear"></div>

    <div class="rowold buttons">
           <?php $this->widget('bootstrap.widgets.TbButton',
                       array('buttonType'=>'submit',
                           'type'=>'primary',
                           'icon'=>'ok white',
                           'label'=> $model->isNewRecord ? tc('Add') : tc('Save'),
                       )); ?>
   	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->