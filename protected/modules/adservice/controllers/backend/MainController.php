<?php
/**********************************************************************************************
*                            CMS Open Real Estate
*                              -----------------
*	version				:	1.9.1
*	copyright			:	(c) 2014 Monoray
*	website				:	http://www.monoray.ru/
*	contact us			:	http://www.monoray.ru/contact
*
* This file is part of CMS Open Real Estate
*
* Open Real Estate is free software. This work is licensed under a GNU GPL.
* http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*
* Open Real Estate is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
* Without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
***********************************************************************************************/

class MainController extends ModuleAdminController{
	public $modelName = 'AdService';

	public function actionView($id){
		$this->redirect(array('admin'));
	}
	public function actionIndex(){
		$this->redirect(array('admin'));
	}

	public function actionAdmin(){
		$this->getMaxSorter();
		parent::actionAdmin();
	}

	public function actionCreate(){
		$model = new $this->modelName;

		$this->performAjaxValidation($model);

		if(isset($_POST[$this->modelName])){
			$model->attributes=$_POST[$this->modelName];
			if($model->validate()) {

				$model->iconUpload = CUploadedFile::getInstance($model, 'image');
				if ($model->iconUpload) {
					$iconUploadPath = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$model->iconsMapPath.'/';

					//$model->icon_file = $model->iconUpload->name;
					$model->image = md5(uniqid()).'.'.$model->iconUpload->extensionName;

					// загружаем и ресайзим иконку
					$model->iconUpload->saveAs($iconUploadPath.$model->image);

					Yii::import('application.extensions.image.Image');
					$icon = new Image($iconUploadPath.$model->image);

					$icon->resize(AdService::MAP_ICON_MAX_WIDTH, AdService::MAP_ICON_MAX_HEIGHT);
					$icon->save();
				}

				if($model->save(false)){
					$this->redirect(array('admin'));
				}
			}
		}

		$this->render('create',array_merge(
			array('model'=>$model),
			$this->params
		));
	}

	public function actionUpdate($id){
		$model = $this->loadModel($id);

		$this->performAjaxValidation($model);

		if(isset($_POST[$this->modelName])){
			$isUploadIcon = false;

			$iconUploadPath = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$model->iconsMapPath.'/';
			$model->iconUpload = CUploadedFile::getInstance($model, 'image');

			if ($model->iconUpload)
				$isUploadIcon = true;

			if ($isUploadIcon) {
				if ($model->image) { // если уже есть - удаляем старую иконку
					$oldIconPath = $iconUploadPath.$model->image;
					if (file_exists($oldIconPath)) {
						@unlink($oldIconPath);
					}
				}
			}

			$model->attributes=$_POST[$this->modelName];

			if($model->validate()) {
				if ($isUploadIcon) {
					//$model->icon_file = $model->iconUpload->name;
					$model->image = md5(uniqid()).'.'.$model->iconUpload->extensionName;

					// загружаем и ресайзим иконку
					$model->iconUpload->saveAs($iconUploadPath.$model->image);

					Yii::import('application.extensions.image.Image');
					$icon = new Image($iconUploadPath.$model->image);

					$icon->resize(AdService::MAP_ICON_MAX_WIDTH, AdService::MAP_ICON_MAX_HEIGHT);
					$icon->save();
				}

				if($model->save(false)){
					$this->redirect(array('admin'));
				}
			}
		}

		$this->render('update',
			array_merge(
				array('model'=>$model),
				$this->params
			)
		);
	}

    public function actionDelete($id){

        // Не дадим удалить последний тип
        if(ApartmentObjType::model()->count() <= 1){
            if(!isset($_GET['ajax'])){
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            Yii::app()->end();
        }

        parent::actionDelete($id);
    }

	public function actionDeleteIcon($id = null) {
	    if ($id) {
		 	$model = $this->loadModel($id);
		    if ($model->image) {
			    $iconUploadPath = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.$model->iconsMapPath.'/';

			    $oldIconPath = $iconUploadPath.$model->image;
			    if (file_exists($oldIconPath)) {
				    @unlink($oldIconPath);
			    }

			    $model->image = '';
			    $model->update(array('image'));
		    }
	    }
		$this->redirect(array('update', 'id' => $id));
	}

    public function getMaxSorter(){
        $model = new $this->modelName;
        $maxSorter = Yii::app()->db->createCommand()
            ->select('MAX(sort) as maxSorter')
            ->from($model->tableName())
            ->queryScalar();
        $this->params['maxSorter'] = $maxSorter;
        return $maxSorter;
    }

    public function getMinSorter(){
        $model = new $this->modelName;
        $minSorter = Yii::app()->db->createCommand()
            ->select('MIN(sort) as maxSorter')
            ->from($model->tableName())
            ->queryScalar();
        $this->params['minSorter'] = $minSorter;
        return $minSorter;
    }
}
