<?php
/**********************************************************************************************
 *                            CMS Open Real Estate
 *                              -----------------
 *	version				:	1.9.1
 *	copyright			:	(c) 2014 Monoray
 *	website				:	http://www.monoray.ru/
 *	contact us			:	http://www.monoray.ru/contact
 *
 * This file is part of CMS Open Real Estate
 *
 * Open Real Estate is free software. This work is licensed under a GNU GPL.
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Open Real Estate is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * Without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 ***********************************************************************************************/

class Assist extends ParentModel {
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const MAIN_PAGE_ID = 1;

    const POSITION_BOTTOM = 1;
    const POSITION_TOP = 2;

    public static function getPositionList(){
        return array(
            self::POSITION_BOTTOM => tt('Bottom', 'assist'),
            self::POSITION_TOP => tt('Top', 'assist'),
        );
    }

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{assist}}';
    }

    public function rules() {
        return array(
            array('title, content', 'safe'),
        );
    }

    public function relations(){
        return array(
            'menuPage' => array(self::HAS_MANY, 'Menu', 'pageId'),
            'menuPageOne' => array(self::HAS_ONE, 'Menu', 'pageId'),
        );
    }

    public function seoFields() {
        return array(
            'fieldTitle' => 'title',
            'fieldDescription' => 'body'
        );
    }

    public function behaviors(){
        return array(
            'AutoTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'date_created',
                'updateAttribute' => 'date_updated',
            ),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => tt('Page title'),
            'content' => tt('Page body'),
            'date_created' => tt('Creation date'),
        );
    }

    public function getUrl() {
        if(issetModule('seo') && param('genFirendlyUrl')){
            $seo = SeoFriendlyUrl::getForUrl($this->id, 'Assist');

            if($seo){
                $field = 'url_'.Yii::app()->language;
                if($seo->direct_url){
                    return Yii::app()->getBaseUrl(true) . '/' . $seo->$field . ( param('urlExtension') ? '.html' : '' );
                }
                return Yii::app()->createAbsoluteUrl('/assist/main/view', array(
                    'url' => $seo->$field . ( param('urlExtension') ? '.html' : '' ),
                ));
            }
        }

        return Yii::app()->createAbsoluteUrl('/assist/main/view', array(
            'id' => $this->id,
        ));
    }

    public function search() {
        $criteria = new CDbCriteria;

        $titleField = 'title';
        $criteria->compare($titleField, $this->$titleField, true);
        $bodyField = 'content';
        $criteria->compare($bodyField, $this->$bodyField, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
            'pagination' => array(
                'pageSize' => param('adminPaginationPageSize', 20),
            ),
        ));
    }

//    public static function getWidgetOptions($widget = null){
//        $arrWidgets =  array(
//            '' => tc('No'),
//            'news' => tc('News'),
//            'apartments' => tc('Listing'),
//            'viewallonmap' => tc('Search for listings on the map'),
//            'contactform' => tc('The form of the section "Contact Us"'),
//            'randomapartments' => tc('Listing (random)'),
//            'specialoffers' => tc('Special offers'),
//        );
//
//        if ($widget && array_key_exists($widget, $arrWidgets))
//            return $arrWidgets[$widget];
//
//        return $arrWidgets;
//    }

    public static function getInfoPagesAddList() {
        $return = array();
        $result = Assist::model()->findAll();
        if ($result) {
            foreach($result as $item) {
                $return[$item->id] = $item->title;
            }
        }

        return $return;
    }





    public function beforeSave(){
        return parent::beforeSave();
    }


    public function afterSave() {
        if(issetModule('seo') && param('genFirendlyUrl')){
            SeoFriendlyUrl::getAndCreateForModel($this);
        }
        return parent::afterSave();
    }

    public function beforeDelete() {

        return parent::beforeDelete();
    }

    private $_filter;

    public function getCriteriaForAdList(){
        $criteria = new CDbCriteria();
        if($this->widget_data){
            $this->_filter = CJSON::decode($this->widget_data);

            if(issetModule('location') && param('useLocation', 1)){
                $this->setForCriteria($criteria, 'country_id', 'loc_country');
                $this->setForCriteria($criteria, 'region_id', 'loc_region');
                $this->setForCriteria($criteria, 'city_id', 'loc_city');
            } else {
                $this->setForCriteria($criteria, 'city_id', 'city_id');
            }

            $this->setForCriteria($criteria, 'type', 'type');
            $this->setForCriteria($criteria, 'obj_type_id', 'obj_type_id');
        }

        //deb($criteria);

        return $criteria;
    }

    private function setForCriteria($criteria, $key, $field){
        if(isset($this->_filter[$key]) && $this->_filter[$key]){
            $criteria->compare($field, $this->_filter[$key]);
        }
    }

    public function scopes() {
        return array(
            'byid' => array('order' => 'id ASC'),
        );
    }
}