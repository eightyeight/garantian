<?php
$this->pageTitle=Yii::app()->name . ' - ' . AssistModule::t('Add infopage');

$this->menu = array(
    array('label' => tt('Manage Assist'), 'url' => array('admin')),
);

$this->adminTitle = AssistModule::t('Add service');
?>

<?php echo $this->renderPartial('/backend/_form', array('model'=>$model)); ?>