<?php
$this->pageTitle=Yii::app()->name . ' - ' . AssistModule::t('Edit infopage');

$this->menu = array(
    array('label' => tt('Manage Assist'), 'url' => array('admin')),
	array('label' => AssistModule::t('Add infopage'), 'url' => array('create')),
	array('label' => tt('Delete infopage'),
		'url'=>'#',
		'linkOptions'=>array(
			'submit'=>array('delete'),
			'confirm'=> tc('Are you sure you want to delete this item?')
		),
	)
);
$this->adminTitle = AssistModule::t('Edit infopage');
?>

<?php echo $this->renderPartial('/backend/_form', array('model'=>$model)); ?>