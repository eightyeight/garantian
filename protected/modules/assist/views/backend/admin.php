<?php
$this->pageTitle=Yii::app()->name . ' - ' . AssistModule::t('Manage Assist');


$this->menu = array(
	array('label' => AssistModule::t('Add infopage'), 'url' => array('create')),
);
$this->adminTitle = AssistModule::t('Manage Assist');
?>

<div class="flash-notice"><?php echo tt('help_Assist_backend_main_admin'); ?></div>

<?php $this->widget('CustomGridView', array(
	'id'=>'assist-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'function(){$("a[rel=\'tooltip\']").tooltip(); $("div.tooltip-arrow").remove(); $("div.tooltip-inner").remove();}',
	'columns'=>array(
		array(
			'class'=>'CCheckBoxColumn',
			'id'=>'itemsSelected',
			'selectableRows' => '2',
			'htmlOptions' => array(
				'class'=>'center',
			),
		),

		array(
			'header' => tc('Name'),
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::encode($data->title)'
		),
		array(
			'header' => tt('Link', 'menumanager'),
			'type'=>'raw',
			'value'=>'$data->getUrl()',
			'filter' => false,
			'sortable' => false,
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'deleteConfirmation' => tc('Are you sure you want to delete this item?'),
			'template'=>'{view}{update}{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => 'true',
				),
			),
		),
	),
));
/*
$this->renderPartial('//site/admin-select-items', array(
	'url' => '/Assist/backend/main/itemsSelected',
	'id' => 'Assist-grid',
	'model' => $model,
	'options' => array(
		'delete' => Yii::t('common', 'Delete')
	),
));*/
?>