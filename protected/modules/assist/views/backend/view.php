<?php
$this->menu = array(
	array('label' => AssistModule::t('Add infopage'), 'url' => array('create')),
	array('label' => AssistModule::t('Edit infopage'), 'url' => array('update', 'id' => $model->id)),
	array('label' => tt('Delete infopage'), 'url' => '#',
		'url'=>'#',
		'linkOptions'=>array(
			'confirm'=> tc('Are you sure you want to delete this item?')
		),
	),
);

$this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes'=>array(
		'id',
		array (
			'label' => CHtml::encode($model->getAttributeLabel('title')),
			'type' => 'raw',
			'value' => CHtml::encode($model->title),
			'template' => "<tr class=\"{class}\"><th>{label}</th><td>{value}</td></tr>\n"
		),
		array (
			'label' => CHtml::encode($model->getAttributeLabel('content')),
			'type' => 'raw',
			'value' => CHtml::decode($model->content),
			'template' => "<tr class=\"{class}\"><th>{label}</th><td>{value}</td></tr>\n"
		),
	),
));
