<div class="form">
	<?php
		$form=$this->beginWidget('CustomForm', array(
			'id'=>'About-form',
			'enableClientValidation'=>false,
		));
	?>

	<p class="note">
		<?php echo Yii::t('common', 'Fields with <span class="required">*</span> are required.'); ?>
	</p>

	<?php echo $form->errorSummary($model); ?>


	<?php echo
		$form->textField($model, 'title', array());
	?>
	<div class="clear"></div>

    <div class="rowold">
        <?php echo $form->labelEx($model,'content'); ?>
        <?php echo $form->textArea($model,'content', array('class' => 'width500 height100')); ?>
        <div class="clear"></div>
        <?php echo $form->error($model,'content'); ?>
    </div>

    <div class="rowold buttons">
        <?php $this->widget('bootstrap.widgets.TbButton',
            array('buttonType'=>'submit',
                'type'=>'primary',
                'icon'=>'ok white',
                'label'=> $model->isNewRecord ? tc('Add') : tc('Save'),
            )); ?>
    </div>
    <?php $this->endWidget();?>
</div><!-- form -->

