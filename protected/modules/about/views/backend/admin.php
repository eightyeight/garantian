<?php
$this->pageTitle=Yii::app()->name . ' - ' . AboutModule::t('Manage About');


$this->menu = array(
	array('label' => AboutModule::t('Add infopage'), 'url' => array('create')),
);
$this->adminTitle = AboutModule::t('Manage About');
?>

<div class="flash-notice"><?php echo tt('help_About_backend_main_admin'); ?></div>

<?php $this->widget('CustomGridView', array(
	'id'=>'About-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'function(){$("a[rel=\'tooltip\']").tooltip(); $("div.tooltip-arrow").remove(); $("div.tooltip-inner").remove();}',
	'columns'=>array(
		array(
			'class'=>'CCheckBoxColumn',
			'id'=>'itemsSelected',
			'selectableRows' => '2',
			'htmlOptions' => array(
				'class'=>'center',
			),
		),

		array(
			'header' => tc('Name'),
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::encode($data->title)'
		),
		array(
			'header' => tt('Link', 'menumanager'),
			'type'=>'raw',
			'value'=>'$data->getUrl()',
			'filter' => false,
			'sortable' => false,
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'deleteConfirmation' => tc('Are you sure you want to delete this item?'),
			'template'=>'{view}{update}{delete}',
			'buttons' => array(
				'delete' => array(
					'visible' => 'true',
				),
			),
		),
	),
));
/*
$this->renderPartial('//site/admin-select-items', array(
	'url' => '/About/backend/main/itemsSelected',
	'id' => 'About-grid',
	'model' => $model,
	'options' => array(
		'delete' => Yii::t('common', 'Delete')
	),
));*/
?>