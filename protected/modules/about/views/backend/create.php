<?php
$this->pageTitle=Yii::app()->name . ' - ' . AboutModule::t('Add infopage');

$this->menu = array(
    array('label' => tt('Manage About'), 'url' => array('admin')),
);

$this->adminTitle = AboutModule::t('Add service');
?>

<?php echo $this->renderPartial('/backend/_form', array('model'=>$model)); ?>