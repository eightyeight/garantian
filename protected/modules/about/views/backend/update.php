<?php
$this->pageTitle=Yii::app()->name . ' - ' . AboutModule::t('Edit infopage');

$this->menu = array(
    array('label' => tt('Manage About'), 'url' => array('admin')),
	array('label' => AboutModule::t('Add infopage'), 'url' => array('create')),
	array('label' => tt('Delete infopage'),
		'url'=>'#',
		'linkOptions'=>array(
			'submit'=>array('delete','id'=>$model->id),
			'confirm'=> tc('Are you sure you want to delete this item?')
		),
	)
);
$this->adminTitle = AboutModule::t('Edit infopage');
?>

<?php echo $this->renderPartial('/backend/_form', array('model'=>$model)); ?>